# WaterFox automatic install for Linux

Automated installation of the WaterFox web browser for The Linux Operating System.

## Project objective

The objective is to provide a method to easily install the WaterFox web browser directly from WaterFox's website and enable WaterFox's automatic update feature for the latest releases. Providing a pure stock WaterFox experience for everyone using your Linux computer at home. 

## Getting started

1. [**Download** the latest release](https://gitlab.com/Linux-Is-Best/Waterfox-automatic-install-for-Linux/-/releases)
1. Extract the archive.
1. Open a terminal window inside the newly extracted folder (directory).
1. Follow the directions below.

In the terminal window run the following command:

```bash
chmod +x Setup.sh
```

This will make the script executable (allowing it to run).  Next, execute the script by typing the following.

```bash
bash Setup.sh
```
   
   (Or your perfered shell, for example, dash, zsh, ksh - POSIX compatible)

Please follow the easy menu prompts and complete the setup.

   Happy Browsing!


## Thunderbird Automatic Install for Linux

If you like WaterFox Automatic Install for Linux, you may also like, [Thunderbird Automatic Install for Linux ](https://gitlab.com/Linux-Is-Best/Thunderbird-automatic-install-for-Linux)
