#!/bin/sh
#
# WaterFox automatic uninstall for Linux
# v1.0.0
#
# That which is done, cannot be undone. Reinstalled, of course! But not undone.

display_menu() {
  printf -- '\n%s\n' " ";
  printf -- '%s\n' "   WaterFox automatic install for Linux" \
    " " \
    "           P E R S O N A L - U N I N S T A L L" \
    " " \
    " CAUTION - You are about to remove and delete" \
    "           WaterFox from your computer!" \
    " " \
    " 1. Uninstall WaterFox" \
    " 2. Purge WaterFox and ALL personal files (Cannot be undone)" \
    " 3. Exit" \
    "" ""
  printf " Please enter option [1 - 3]: ";
}

uninstall_edition() {
  edition="$1"
  script_path="./personal/uninstallers/un${edition}.sh"

  if [ -f "$script_path" ]; then
    clear
    printf -- '\n%s\n\n' "You selected $edition"
    chmod +x "$script_path"
    "$script_path"
  else
    clear
    printf -- '\n%s\n\n' "Script not found: $script_path"
  fi
}

purge_waterfox() {
  clear
  printf -- '\n%s\n\n' "You selected to purge WaterFox and ALL personal files. This cannot be undone!"
  printf "Are you sure you want to proceed? [y/n]: "
  read -r answer

  if [ "$answer" = "y" ] || [ "$answer" = "Y" ]; then
    chmod +x "./personal/uninstallers/personal_purge.sh"
    "./personal/uninstallers/personal_purge.sh"
  else
    printf -- '\n%s\n\n' "Purge operation cancelled. WaterFox and personal files have not been removed."
  fi
}

# Main script execution
while true; do
  display_menu
  read -r opt

  case $opt in
    [1])
      case $opt in
        1) uninstall_edition "WaterFox" ;;
        # 2) uninstall_edition "Beta" ;;
        # 3) uninstall_edition "Developer" ;;
        # 4) uninstall_edition "Nightly" ;;
        # 5) uninstall_edition "Extended" ;;
      esac
      break
      ;;

    2)
      purge_waterfox
      break
      ;;

    3)
      clear
      printf -- '\n%s\n\n' "Goodbye, $USER"
      exit 0
      ;;

    *)
      clear
      printf -- '\n\n%s\n' " $opt is an invalid option. Please select an option between 1-3 only" \
        " Press the [enter] key to continue. . ."
      read -r
      clear
      ;;
  esac
done
