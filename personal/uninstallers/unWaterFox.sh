#!/bin/sh
#
# WaterFox Automatic Install for Linux -- Personal Uninstall WaterFox (stable) -- File can be used independently.

# Uninstalling SeaMonkey notice
printf -- '\n%s\n' " Uninstalling your personal copy of WaterFox ";

# Small delay to give user time to read the above notice.
sleep 3;

# Installation
rm -r -f  "$HOME"/WaterFox/waterfox/ ;

# Menu shortcuts
rm -r -f "$HOME"/.local/share/applications/WaterFox.desktop ;

# determine the XDG_DESKTOP_DIR, for multi language support!
DESKTOP_DIR=$(xdg-user-dir DESKTOP)
# Current desktop shortcuts
rm -r -f "$DESKTOP_DIR"/WaterFox.desktop ;

# exit notice
printf -- '%s\n' "" "" "" " Thank you for using WaterFox." \
" WaterFox has been deleted and uninstalled. Per your request." \
" Really sorry to see you go. Hope to see you again real soon." "" ""

# exit
exit 0
