#!/bin/sh
#
# WaterFox Automatic Install for Linux -- System Uninstall 64-bit -- File can be used independently.

# Uninstalling WaterFox notice
printf -- '\n%s\n' " Per your request. Now uninstalling WaterFox.";

# Small delay to give user time to read the above notice.
sleep 3;

# Installation
sudo rm -r -f  /opt/BrowserWorks_WaterFox/ ;

# Menu shortcuts
sudo rm -r -f /usr/share/applications/WaterFox.desktop ;

sudo rm -r -f /etc/skel/Desktop/WaterFox.desktop ;

# determine the XDG_DESKTOP_DIR, then the DESKTOP_NAME for multi language support, this assumes, that every user has the same locale or Desktop Name
DESKTOP_NAME=$(basename "$(sudo xdg-user-dir DESKTOP)")
# Current deskop shortcuts
sudo rm -r -f /home/*/"$DESKTOP_NAME"/WaterFox.desktop ;


# Exit notice.
printf -- '%s\n' "" "" "" " Thank you for using WaterFox." \
" WaterFox has been deleted and uninstalled. Per your request." \
" Really sorry to see you go. Hope to see you again real soon." "" ""

# exit
exit 0
