#!/bin/sh
#
# WaterFox automatic install for Linux
# v1.0.0

# Specify the directory where the WaterFox uninstall scripts are located
SCRIPT_DIR="./64bit/uninstallers"

# Function to uninstall a single WaterFox edition
install_single_edition() {
  edition=$1
  script_path="${SCRIPT_DIR}/${edition}_UNINSTALL.sh"

  if [ -f "$script_path" ]; then
    sudo chmod +x "$script_path"    # Set the script as executable
    exec "$script_path"            # Replace the current process with the sub-script's process
  else
    clear
    printf -- '\n%s\n\n' "Script not found: $script_path"
  fi
}

# Function to purge all
purge_all_editions() {
  clear
  printf -- '\n%s\n\n' "Caution:"
  printf -- '%s\n' "This will remove your WaterFox system install AND all browser cache and configuration files for all copies of WaterFox for all users on your computer (system-wide)."
  printf -- '%s\n' "Are you sure you want to proceed? (Y/N)"
  read -r confirm

  if [ "$confirm" = "Y" ] || [ "$confirm" = "y" ]; then
    chmod +x "${SCRIPT_DIR}/PURGE_64.sh"
    "${SCRIPT_DIR}/PURGE_64.sh"
  else
    printf -- '\n%s\n\n' "Cancelling the PURGE of WaterFox. No changes have been made."
  fi
}

# Function to display the main menu
display_main_menu() {
  clear
  printf -- '\n%s\n' " ";
  printf -- '%s\n' "   WaterFox automatic install for Linux" \
    " " \
    "           S Y S T E M - U N I N S T A L L" \
    " " \
    " CAUTION - You are about to remove and delete" \
    "           WaterFox from your computer!" \
    " " \
    " 1. Uninstall WaterFox" \
    " 2. Exit" \
    " " \
    " 66. PURGE - CAUTION: Will remove WaterFox" \
    "                     and all browser cache and configuration files for" \
    "                     all copies of WaterFox for all users on your computer." \
    "" ""
  printf " Please enter option [1 - 2, 66]: "
}

# Main script execution
while true; do
  display_main_menu
  read -r opt

  case $opt in
    [1])
      case $opt in
        1) edition="WaterFox" ;;
        # 2) edition="Beta" ;;
        # 3) edition="Developer_Edition" ;;
        # 4) edition="Nightly" ;;
        # 5) edition="ESR" ;;
      esac
      install_single_edition "$edition"
      break
      ;;
    66)
      purge_all_editions # Execute order 66
      break
      ;;
    2)
      clear
      printf -- '\n%s\n\n' "Goodbye, $USER"
      exit 1
      ;;
    *)
      clear
      printf -- '\n\n%s\n' " $opt is an invalid option. Please select an option between 1-2 or 66 only" \
        " Press the [enter] key to continue. . ."
      read -r
      ;;
  esac
done
